package net.innospire.tsgen

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.file.Directory
import org.gradle.api.file.DirectoryProperty
import org.gradle.api.model.ObjectFactory
import java.io.File

@Suppress("unused")
class TypescriptGeneratorPlugin : Plugin<Project> {
    override fun apply(target: Project) {
        val extension = target.extensions.create("typescriptGenerator", TypescriptGeneratorPluginExtension::class.java)
        target.tasks.create("generateTs", GenerateTypescriptTask::class.java) {
            it.dependsOn(target.getTasksByName("compileKotlin", false))
            it.inputDir.set(File("${target.buildDir}/classes/kotlin/main"))
            it.outputDir.set(File("src/main/webapp/src/generated"))
            extension.task = it
        }
    }
}

open class TypescriptGeneratorPluginExtension {
    lateinit var task: GenerateTypescriptTask

    @Suppress("unused")
    var outputDir
        get() = task.outputDir
        set(value) {
            task.outputDir.set(value)
        }

    @Suppress("unused")
    var dataClassNamespaces
        get() = task.dataClassNamespaces
        set(value) {
            task.dataClassNamespaces = value
        }

    @Suppress("unused")
    var controllerClassNamespaces
        get() = task.controllerClassNamespaces
        set(value) {
            task.controllerClassNamespaces = value
        }

    @Suppress("unused")
    var ignore
        get() = task.ignore
        set(value) {
            task.ignore = value
        }

    @Suppress("unused")
    var additionalDataClassSuffixes
        get() = task.additionalDataClassSuffixes
        set(value) {
            task.additionalDataClassSuffixes = value
        }

    @Suppress("unused")
    var generateAsClasses
        get() = task.generateAsClasses
        set(value) {
            task.generateAsClasses = value
        }

    @Suppress("unused")
    var version9
        get() = task.version9
        set(value) {
            task.version9 = value
        }

    @Suppress("unused")
    var deriveEndpointFromControllerNamespace
        get() = task.deriveEndpointFromControllerNamespace
        set(value) {
            task.deriveEndpointFromControllerNamespace = value
        }
}
