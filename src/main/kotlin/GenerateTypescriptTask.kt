package net.innospire.tsgen

import me.ntrrgc.tsGenerator.TypeScriptGenerator
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputDirectory
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.TaskAction
import org.springframework.core.io.InputStreamResource
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.http.HttpEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import java.io.File
import java.net.URLClassLoader
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*
import jakarta.persistence.Entity
import jakarta.persistence.EntityManager
import jakarta.servlet.http.HttpServletRequest
import org.gradle.api.file.DirectoryProperty
import org.gradle.work.FileChange
import org.gradle.work.Incremental
import org.gradle.work.InputChanges
import kotlin.NoSuchElementException
import kotlin.reflect.*
import kotlin.reflect.full.isSubclassOf
import kotlin.reflect.full.memberFunctions
import kotlin.reflect.jvm.internal.KotlinReflectionInternalError
import kotlin.reflect.jvm.javaMethod
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.oauth2.core.oidc.user.OidcUser
import org.springframework.web.servlet.ModelAndView

open class GenerateTypescriptTask : DefaultTask() {
    @get:Incremental
    @get:InputDirectory
    val inputDir: DirectoryProperty = project.objects.directoryProperty()

    @get:OutputDirectory
    val outputDir: DirectoryProperty = project.objects.directoryProperty()

    @Input
    var nestingLevel = 3

    @Input
    var endpoint: String? = null

    @Input
    var dataClassNamespaces: List<String> = emptyList()

    @Input
    var controllerClassNamespaces: List<String> = emptyList()

    @Input
    var ignore: List<String> = emptyList()

    @Input
    var additionalDataClassSuffixes: List<String> = emptyList()

    @Input
    var generateAsClasses = false

    @Input
    var version9 = false

    @Input
    var deriveEndpointFromControllerNamespace = false

    @TaskAction
    fun generate(inputs: InputChanges) {
        if (!inputs.isIncremental) {
            project.delete(outputDir.get().asFile.listFiles())
        }

        val classLoader = URLClassLoader(arrayOf(
                inputDir.get().asFile.toURI().toURL(),
                RestController::class.java.protectionDomain.codeSource.location,
                InputStreamResource::class.java.protectionDomain.codeSource.location,
                Function0::class.java.protectionDomain.codeSource.location,
                EntityManager::class.java.protectionDomain.codeSource.location,
                HttpServletRequest::class.java.protectionDomain.codeSource.location,
                JpaRepository::class.java.protectionDomain.codeSource.location,
                PagingAndSortingRepository::class.java.protectionDomain.codeSource.location,
                UserDetails::class.java.protectionDomain.codeSource.location,
                OidcUser::class.java.protectionDomain.codeSource.location,
                ModelAndView::class.java.protectionDomain.codeSource.location,
                @OptIn(ExperimentalStdlibApi::class)
                kotlin.enums.EnumEntries::class.java.protectionDomain.codeSource.location,
        ))

        val restControllerAnnotationClass = classLoader.loadAnnotationClass<RestController>()
        val requestMappingAnnotationClasses = listOf(
                classLoader.loadAnnotationClass<RequestMapping>(),
                classLoader.loadAnnotationClass<GetMapping>(),
                classLoader.loadAnnotationClass<PostMapping>(),
                classLoader.loadAnnotationClass<PutMapping>(),
                classLoader.loadAnnotationClass<PatchMapping>(),
                classLoader.loadAnnotationClass<DeleteMapping>()
        )
        val requestBodyAnnotationClass = classLoader.loadAnnotationClass<RequestBody>()
        val requestParamAnnotationClass = classLoader.loadAnnotationClass<RequestParam>()
        val pathVariableAnnotationClass = classLoader.loadAnnotationClass<PathVariable>()

        val generated = LinkedHashSet<String>()

        for (change in inputs.getFileChanges(inputDir)) {
            if (change.file.extension != "class") continue
            if (change.file.nameWithoutExtension.endsWith("Kt")) continue
            if (change.file.nameWithoutExtension.contains("$")) continue
            val relativeInputFile = getRelativeInputFile(change)
            val namespace = getNamespace(relativeInputFile) ?: continue
            try {
                if (dataClassNamespaces.any { namespace.startsWith(it) } && !ignore.any {namespace.startsWith(it)}) {
                    println("out of date: ${relativeInputFile.name}")
                    val dataClass = loadClass(relativeInputFile, classLoader, namespace)
                    if (ignore.any { dataClass.canonicalName.startsWith(it) }) {
                        continue
                    }
                    val individualDefinitions = try {
                        TypeScriptGenerator(
                            rootClasses = setOf(
                                dataClass.kotlin
                            ),
                            mappings = mapOf(
                                LocalDateTime::class to "(Date | null)",
                                LocalDate::class to "(Date | null)",
                                Instant::class to "(Date | null)",
                                Date::class to "(Date | null)",
                                UUID::class to "string"
                            )
                        ).individualDefinitions
                    } catch (ex: KotlinReflectionInternalError) {
                        throw IllegalStateException("Kotlin reflection error during typescript generation. Are you using Date instead of LocalDate?", ex)
                    }
                    val illegalReferences = individualDefinitions.asSequence()
                        .filter { definition ->
                            val (definitionKind, definitionName) = definition.split(" ", "<")
                            definitionKind != "type" &&
                                    !definitionName.endsWith("Data") &&
                                    !definitionName.endsWith("Value")&&
                                    !definitionName.endsWith("Values") &&
                                    !additionalDataClassSuffixes.any { definitionName.endsWith(it) }
                        }.map { it.split(" ")[1] }.toList()
                    if (illegalReferences.any()) {
                        throw IllegalStateException("${illegalReferences.joinToString()} referenced by ${dataClass.simpleName} must be enum or data class.")
                    }
                    val unionTypes = individualDefinitions.asSequence()
                        .filter { definition ->
                            val definitionKind = definition.split(" ")[0]
                            definitionKind == "type"
                        }.map { it.split(" ")[1] }.toList()
                    individualDefinitions.forEach { definition ->
                        val definitionName = definition.split(" ", "<")[1]
                        if (definitionName in generated) return@forEach
                        val targetFile = File(outputDir.get().asFile, "data/${getDataClassTypescriptFileNameWithoutExtension(definitionName)}.ts")
                        println("generated: ${targetFile.name}")
                        targetFile.parentFile.mkdirs()
                        val imports = individualDefinitions
                            .asSequence()
                            .map { it.split(" ", "<")[1] }
                            .minus(definitionName)
                            .filter { Regex("\\b$it\\b").containsMatchIn(definition) }
                            .distinct()
                            .joinToString("\n") {
                                "import {$it} from \"./${getDataClassTypescriptFileNameWithoutExtension(it)}\""
                            }
                        val fixedDefinition = unionTypes.fold(definition) { original, unionType ->
                            original.replace("[key: $unionType]:", "[key in $unionType]:")
                        }.let {
                            if (generateAsClasses && it.startsWith("interface")) it
                                .replaceFirst("interface", "class")
                                .lines().joinToString("\n") {
                                    if (it.endsWith("Data;")) it
                                        .replace(":", " = new")
                                        .replace(";", "();")
                                    else it.replace(";", " = null;")
                                }
                            else it.lines().joinToString("\n") {
                                if (it.endsWith(" | null;")) it
                                    .replaceFirst(":", "?:")
                                else it
                            }
                        }

                        targetFile.writeText("$imports\n\nexport $fixedDefinition".trim())
                        val indexFile = File(outputDir.get().asFile, "data/index.ts")
                        val indexContent = if (indexFile.exists()) indexFile.readText() else ""
                        val indexEntry = "export * from \"./${getDataClassTypescriptFileNameWithoutExtension(definitionName)}\""
                        if (indexEntry !in indexContent) {
                            if (indexFile.createNewFile()) {
                                indexFile.writeText(indexEntry)
                            } else {
                                indexFile.appendText("\n$indexEntry")
                            }
                        }
                        generated.add(definitionName)
                    }
                } else if (controllerClassNamespaces.any{ namespace.startsWith(it) } && !ignore.any { namespace.startsWith(it) }) {
                    val controllerClass = loadClass(relativeInputFile, classLoader, namespace)
                    controllerClass.getAnnotation(restControllerAnnotationClass)
                        ?: continue
                    if (ignore.any { controllerClass.canonicalName.startsWith(it) }) {
                        continue
                    }
                    println("out of date: ${relativeInputFile.name}")
                    val resourceName = controllerClass.simpleName.replace("Controller", "Resource")
                    var resourceText = """
    /**
     * This resource class was generated from Controller ${controllerClass.name}
     */
    
    @Injectable()
    @ResourceParams({})
    export class $resourceName extends ApiResource {
    """
                    val imports = HashSet<String>()

                    for (controllerFunction in controllerClass.kotlin.memberFunctions) {

                        if (ignore.any { "${controllerClass.canonicalName}.${controllerFunction.name}".startsWith(it) }) { continue }

                        val mappingAnnotation = requestMappingAnnotationClasses
                            .asSequence()
                            .mapNotNull { controllerFunction.javaMethod?.getAnnotation(it) }
                            .singleOrNull() ?: continue

                        val path = mappingAnnotation[RequestMapping::value].singleOrNull()
                            ?: throw NoSuchElementException("No path specified for @RequestMapping of ${controllerClass.simpleName}.${controllerFunction.name}")
                        val method = try {
                            mappingAnnotation.get<RequestMapping, Array<*>>(RequestMapping::method)
                                .map { it as Enum<*> }
                                .singleOrNull()
                                ?.reflectName()
                                ?.let { RequestMethod.valueOf(it) }
                        } catch (ex: NoSuchMethodException) {
                            RequestMethod.valueOf(mappingAnnotation.annotationClass.simpleName!!
                                .replace("Mapping", "").uppercase())
                        } ?: RequestMethod.GET
                        println("-> $method $path (${controllerFunction.name})")

                        val requestBodyType: String
                        val requestParams: List<String>
                        val useFormData: Boolean

                        if(controllerFunction.parameters.any { parameter ->
                                parameter.annotations.any { requestParamAnnotationClass.isInstance(it)} &&
                                        getTypescriptTypeName(parameter.type, classLoader, imports) in listOf("File", "File[]")
                            }) {
                            useFormData = true
                            requestParams = emptyList() // request params are sent in body
                            requestBodyType = "{" + getRequestParamsFromControllerFunction(
                                controllerFunction,
                                requestParamAnnotationClass,
                                classLoader,
                                imports
                            ).joinToString() + "}"
                        } else {
                            useFormData = false
                            requestBodyType = controllerFunction.parameters.singleOrNull { parameter ->
                                parameter.annotations.any { requestBodyAnnotationClass.isInstance(it) }
                            }?.let { getTypescriptTypeName(it.type, classLoader, imports) } ?: "null"

                            requestParams = getRequestParamsFromControllerFunction(
                                controllerFunction,
                                requestParamAnnotationClass,
                                classLoader,
                                imports
                            )
                        }

                        val pathVariables = controllerFunction.parameters
                            .asSequence()
                            .mapNotNull { parameter ->
                                parameter.annotations
                                    .singleOrNull { pathVariableAnnotationClass.isInstance(it) }
                                    ?.let { parameter to it }
                            }.map { (parameter, annotation) ->
                                val parameterName = annotation[PathVariable::value].let {
                                    if (it.isBlank()) parameter.name else it
                                }
                                val parameterType = getTypescriptTypeName(parameter.type, classLoader, imports)
                                "$parameterName: $parameterType"
                            }
                            .toList()

                        val responseBodyType = getTypescriptTypeName(controllerFunction.returnType, classLoader, imports)
                        //keep in sync with JsonWrappingReturnValueHandler/supportsReturnType
                        val isJsonWrapped =
                            responseBodyType in listOf("boolean", "number", "string", "Date") ||
                                    responseBodyType.startsWith("('") // Enum: ('...
                        val actualResponseBodyType =
                            if (isJsonWrapped) "JsonWrappedValue<$responseBodyType>"
                            else responseBodyType

                        if (isJsonWrapped) imports.add("JsonWrappedValue")

                        val endpoint = if(deriveEndpointFromControllerNamespace) {
                            namespace.split(".controller").first()
                        } else endpoint

                        resourceText += """
        @ResourceAction({
            method: ResourceRequestMethod.${method.toString().lowercase().capitalize()},
            path: '/${path.trimStart('/')}'${if (endpoint.isNullOrBlank()) "" else ",\n        endpoint: '$endpoint'"}${if (requestBodyType.endsWith("[]")) ",\n        keepEmptyBody: true" else ""}${if (useFormData) ",\n        requestBodyType: ResourceRequestBodyType.FORM_DATA" else ""}
        })
        private _${controllerFunction.name}: ${if (version9) "IResourceMethodObservableStrict" else "IResourceMethodStrict"}<$requestBodyType, ${if (requestParams.isEmpty()) "null" else "{${requestParams.joinToString()}}"}, ${if (pathVariables.isEmpty()) "null" else "{${pathVariables.joinToString()}}"}, $actualResponseBodyType> | undefined
        ${controllerFunction.name}(${listOfNotNull(
                            if (requestBodyType == "null") null else "requestBody: $requestBodyType",
                            if (pathVariables.isEmpty()) null else if (pathVariables.size == 1) pathVariables.single() else "pathVariables: {${pathVariables.joinToString()}}",
                            if (requestParams.isEmpty()) null else "requestParams${if (requestParams.all { '?' in it }) "?" else ""}: {${requestParams.joinToString()}}"
                        ).joinToString()}): Promise<$responseBodyType> {
            if (!this._${controllerFunction.name}) throw new Error("resource has not been properly initialized")
            return this._${controllerFunction.name}(${listOf(
                            if (requestBodyType == "null") "null" else "requestBody",
                            if (requestParams.isEmpty()) "null" else if (requestParams.all { '?' in it }) "requestParams || {}" else "requestParams",
                            if (pathVariables.isEmpty()) "null" else if (pathVariables.size == 1) pathVariables.single().split(':').first().let { "{$it: $it}" } else "pathVariables"
                        ).joinToString()})${if (version9) ".toPromise()" else ""}${if (isJsonWrapped) ".then((result) => result.value)" else ""}
        }
    """
                    }

                    val headerText = """
    import {Injectable} from "@angular/core";
    import {${if (version9) "IResourceMethodObservableStrict" else "IResourceMethodStrict"}, ResourceAction, ResourceParams, ResourceRequestBodyType, ResourceRequestMethod} from "@ngx-resource/core";
    import {ApiResource} from "${"../".repeat(nestingLevel)}api-resource";
    import {${imports.joinToString()}} from "../data"
    """
                    resourceText = "$headerText$resourceText\n}"

                    val targetModuleName = "${resourceName.replace("Resource", "").toKebabCase()}.resource"
                    val targetFile = File(outputDir.get().asFile, "resources/$targetModuleName.ts")
                    println("generated: ${targetFile.name}")
                    targetFile.parentFile.mkdirs()
                    targetFile.writeText(resourceText.trim())
                    val indexFile = File(outputDir.get().asFile, "resources/index.ts")
                    val indexContent = if (indexFile.exists()) indexFile.readText() else ""
                    val indexEntry = "export * from \"./${getDataClassTypescriptFileNameWithoutExtension(targetModuleName)}\""
                    if (indexEntry !in indexContent) {
                        if (indexFile.createNewFile()) {
                            indexFile.writeText(indexEntry)
                        } else {
                            indexFile.appendText("\n$indexEntry")
                        }
                    }
                }
            } catch (t: Throwable) {
                when (t) {
                    is ClassNotFoundException,
                    is NoClassDefFoundError -> throw IllegalStateException("Usage of class ${t.message?.replace('/', '.')} in ${relativeInputFile.nameWithoutExtension} is not allowed.")

                    else -> throw t
                }
            }
        }
    }

    private fun getRequestParamsFromControllerFunction(
            controllerFunction: KFunction<*>,
            requestParamAnnotationClass: Class<out Annotation>,
            classLoader: URLClassLoader,
            imports: HashSet<String>
    ): List<String> {
        return controllerFunction.parameters
                .asSequence()
                .mapNotNull { parameter ->
                    parameter.annotations
                            .singleOrNull { requestParamAnnotationClass.isInstance(it) }
                            ?.let { parameter to it }
                }.map { (parameter, annotation) ->
                    val parameterName = annotation[RequestParam::name].let {
                        if (it.isBlank()) parameter.name else it
                    }
                    val required = annotation[RequestParam::required]
                    val parameterType = getTypescriptTypeName(parameter.type, classLoader, imports)
                    "$parameterName${if (required) "" else "?"}: $parameterType"
                }.toList()
    }

    private fun getDataClassTypescriptFileNameWithoutExtension(definitionName: String) = definitionName.replace(Regex("<\\w+>"), "").toKebabCase()

    private fun loadClass(relativeInputFile: File, classLoader: URLClassLoader, namespace: String): Class<*> {
        val className = relativeInputFile.nameWithoutExtension
        return classLoader.loadClass("$namespace.$className")
    }

    private fun getNamespace(relativeInputFile: File) =
            relativeInputFile.parent
                    ?.replace('\\', '.')
                    ?.replace('/', '.')

    private fun getRelativeInputFile(change: FileChange) =
            change.file.relativeTo(inputDir.get().asFile)

    private fun getTypescriptTypeName(kotlinType: KType, classLoader: ClassLoader, imports: MutableSet<String>): String {
        val classifier = kotlinType.classifier
        return when (classifier) {
            // classLoader required for non-primitive/java types
            Any::class -> "any"
            classLoader[Unit::class] -> "null"
            Boolean::class -> "boolean"
            Long::class, Int::class, Double::class -> "number"
            String::class, UUID::class -> "string"
            Date::class, LocalDate::class, LocalDateTime::class -> "Date"
            List::class, Set::class -> "${getTypescriptTypeName(kotlinType.arguments.single().type!!, classLoader, imports)}[]"
            Map::class -> "Map<${getTypescriptTypeName(kotlinType.arguments.first().type!!, classLoader, imports)}, ${getTypescriptTypeName(kotlinType.arguments.last().type!!, classLoader, imports)}>"
            ByteArray::class -> "ArrayBuffer"
            //Map::class -> "{ [${getTypescriptTypeName(kotlinType.arguments.first().type!!, imports)}]: ${getTypescriptTypeName(kotlinType.arguments.last().type!!, imports)} }"
            classLoader[HttpEntity::class] -> "Blob"
            classLoader[MultipartFile::class] -> "File"
            is KClass<*> -> {
                when {
                    dataClassNamespaces.any { classifier.qualifiedName!!.startsWith("$it.") } -> {
                        imports.add(classifier.simpleName!!)
                        val typeArgs = if (kotlinType.arguments.none()) "" else "<${kotlinType.arguments.joinToString { getTypescriptTypeName(it.type!!, classLoader, imports) }}>"
                        classifier.simpleName!! + typeArgs
                    }
                    classifier.annotations.any { classLoader.loadAnnotationClass<Entity>().isInstance(it) } -> {
                        "${classifier.simpleName}Data".also { imports.add(it) }
                    }
                    classifier.isSubclassOf(Enum::class) -> {
                        val values = classifier.java.getMethod("values").invoke(null) as Array<*>
                        "(${values.joinToString(" | ") { "'$it'" }})"
                    }
                    else -> throw NotImplementedError("Cannot represent type '$kotlinType' in TypeScript.")
                }
            }
            is KTypeParameter -> classifier.name
            else -> throw NotImplementedError(kotlinType.toString())
        }
    }
}

private operator fun ClassLoader.get(kClass: KClass<*>) = loadClass(kClass.qualifiedName).kotlin

fun String.toKebabCase(): String = this.foldIndexed("") { i, acc, char ->
    acc + when {
        i == 0 -> char.lowercaseChar()
        char.isUpperCase() -> "-${char.lowercaseChar()}"
        else -> char
    }
}

@Suppress("UNCHECKED_CAST")
inline fun <reified T : Annotation> ClassLoader.loadAnnotationClass() = loadClass(T::class.qualifiedName) as Class<out Annotation>

@Suppress("UNCHECKED_CAST")
inline operator fun <reified T : Annotation, R> Annotation.get(property: KProperty1<T, R>) = javaClass.getMethod(property.name).invoke(this) as R

@Suppress("NOTHING_TO_INLINE")
inline fun Enum<*>.reflectName() = javaClass.getMethod(Enum<*>::name.name).invoke(this) as String
