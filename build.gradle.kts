import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.9.24"
    `java-gradle-plugin`
    id("com.gradle.plugin-publish") version "1.2.0"
}

group = "net.innospire"
version = "1.9.0"

gradlePlugin {
    plugins {
        create("typescriptGeneratorPlugin") {
            id = "net.innospire.typescript-generator"
            displayName = "Typescript Resource Generator"
            description = "Generates Typescript files for ngx-resource from Spring MVC Rest Controllers."
            implementationClass = "net.innospire.tsgen.TypescriptGeneratorPlugin"
            tags = listOf("typescript", "ngx-resource", "rest", "spring-mvc")
        }
    }
    website = "https://bitbucket.org/uniwunder/typescript-generator"
    vcsUrl = "https://bitbucket.org/uniwunder/typescript-generator"
}

repositories {
    mavenCentral()
    maven("https://jitpack.io")
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("reflect"))
    implementation("com.github.ntrrgc", "ts-generator", "1.1.0")
    implementation("org.springframework", "spring-webmvc", "6.0.9")
    implementation("org.springframework.data", "spring-data-jpa", "3.1.0")
    implementation("jakarta.persistence", "jakarta.persistence-api", "3.1.0")
    implementation("org.apache.tomcat.embed", "tomcat-embed-core", "10.1.8")
    implementation("org.springframework.security", "spring-security-core", "6.1.1")
    implementation("org.springframework.security", "spring-security-oauth2-core", "6.1.1")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "21"
        apiVersion = "1.8"
    }
}
